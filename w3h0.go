package main

import (
	"bufio"
	"crypto/sha256"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
)

func main() {
	inFilename, err := filenamesFromCommandLine()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	// inFile, outFile := os.Stdin, os.Stdout
	inFile := os.Stdin
	if inFilename != "" {
		if inFile, err = os.Open(inFilename); err != nil {
			log.Fatal(err)
		}
		defer inFile.Close()
	}
	// if outFilename != "" {
	// 	if outFile, err = os.Create(outFilename); err != nil {
	// 		log.Fatal(err)
	// 	}
	// 	defer outFile.Close()
	// }

	// if err = process(inFile, outFile); err != nil {
	if err = process(inFile); err != nil {
		log.Fatal(err)
	}
}

// func filenamesFromCommandLine() (inFilename, outFilename string, err error) {
func filenamesFromCommandLine() (inFilename string, err error) {
	if len(os.Args) == 1 || len(os.Args) > 1 && (os.Args[1] == "-h" || os.Args[1] == "--help") {
		err = fmt.Errorf("usage: %s file",
			filepath.Base(os.Args[0]))
		return "", err
	}
	if len(os.Args) > 1 {
		inFilename = os.Args[1]
		// if len(os.Args) > 2 {
		// 	outFilename = os.Args[2]
		// }
	}
	// if inFilename != "" && inFilename == outFilename {
	// 	log.Fatal("won't overwrite the infile")
	// }
	// return inFilename, outFilename, nil
	return inFilename, nil
}

// func process(inFile io.Reader, outFile io.Writer) (err error) {
func process(inFile io.Reader) (err error) {
	const BUFSIZE = 1024
	const HASHSIZE = 32
	// buffer := make([]byte, BUFSIZE)
	const MAXFILESIZE = 20 * 1024 * 1024
	var chunk_size, bytes_read, bytes_to_write int //, bytes_written int
	chunk := make([]byte, BUFSIZE+HASHSIZE)
	file := make([]byte, MAXFILESIZE)

	reader := bufio.NewReader(inFile)
	// writer := bufio.NewWriterSize(outFile, BUFSIZE)
	// writer := bufio.NewWriter(outFile)
	// defer func() {
	// 	if err == nil {
	// 		err = writer.Flush()
	// 	}
	// }()

	// reading
	eof := false
	for !eof {
		chunk_size, err = reader.Read(file)
		bytes_read += chunk_size
		if chunk_size != 0 {
			fmt.Printf("Read %d bytes\n", chunk_size)
		}
		if err == io.EOF {
			err = nil  // io.EOF isn't really an error
			eof = true // this will end the loop at the next iteration
		} else if err != nil {
			return err // finish immediately for real errors
		}
	}

	// hash
	right := true
	// right_hash := make([]byte, 32)
	h := sha256.New()

	if bytes_read > 0 {
		fmt.Printf("Hashing %d bytes\n", bytes_read)
		for bytes_to_write = bytes_read; bytes_to_write > 0; bytes_to_write -= chunk_size {
			// fmt.Printf("Bytes to write:\t%4d\n", bytes_to_write)
			chunk_size = bytes_to_write % BUFSIZE
			if chunk_size == 0 {
				chunk_size = BUFSIZE
			}
			// fmt.Printf("Chunk size:\t\t%8d\n", chunk_size)
			if right {
				chunk = file[bytes_to_write-chunk_size : bytes_to_write]
				// fmt.Println("TAIL")
				// fmt.Printf("Chunk (%d bytes):\n%x\n", len(chunk), chunk)
				h = sha256.New()
				io.WriteString(h, string(chunk))
				// fmt.Printf("Hash (%d bytes):\n%x\n", len(h.Sum(nil)), h.Sum(nil))
				//A copy(right_hash, h.Sum(nil))
				// fmt.Printf("Hash (%d bytes):\n%x\n", len(right_hash), right_hash)
				right = false
				// fmt.Println("TAIL END")
			} else {
				// fmt.Println("\nCYCLE")
				// fmt.Printf("Chunk (%d bytes):\n%x\n", len(file[bytes_to_write-chunk_size:bytes_to_write]), file[bytes_to_write-chunk_size:bytes_to_write])
				// fmt.Printf("Hash (%d bytes):\n%x\n", len(right_hash), right_hash)
				chunk = append(file[bytes_to_write-chunk_size:bytes_to_write], h.Sum(nil)...)
				// fmt.Printf("Expanded chunk_size (%d bytes):\n%x\n", len(chunk), chunk)
				h = sha256.New()
				io.WriteString(h, string(chunk))
				//A copy(right_hash, h.Sum(nil))
				// fmt.Println("CYCLE END")
			}
			// fmt.Printf("Chunk (%d bytes):\n%x\n", len(chunk), chunk)
			// fmt.Printf("F: %04d-%04d, H: %x\n\n", bytes_to_write-chunk_size, bytes_to_write, h.Sum(nil))

		}
		fmt.Printf("Hash (%d bytes):\n%x\n", len(h.Sum(nil)), h.Sum(nil))
		// if bytes_written, err = writer.Write(file); err != nil {
		//  return err
		// }
		// fmt.Printf("R:%d / W:%d\n", bytes_read, bytes_written)
	}

	// writing

	return nil
}
