package main

import "fmt"

func main() {
	for i := 0; i < 11; i++ {
		fmt.Printf("Root(%d) = %d\n", i, (i*i*i)%11)
	}
}
