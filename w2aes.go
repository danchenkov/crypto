package main

import (
	"crypto/aes"
	"crypto/cipher"
	// "crypto/rand"
	"fmt"
	// "io"
	"encoding/hex"
	"log"
)

// CORRECT ANSWERS:
// Basic CBC mode encryption needs padding.
// Our implementation uses rand. IV
// CTR mode lets you build a stream cipher from a block cipher.
// Always avoid the two time pad!

func main() {

	plaintext := make([]byte, 64)
	// 1
	// ciphertext, err := hex.DecodeString("4ca00ff4c898d61e1edbf1800618fb2828a226d160dad07883d04e008a7897ee2e4b7465d5290d0c0e6c6822236e1daafb94ffe0c5da05d9476be028ad7c1d81")
	// 2
	// ciphertext, err := hex.DecodeString("5b68629feb8606f9a6667670b75b38a5b4832d0f26e1ab7da33249de7d4afc48e713ac646ace36e872ad5fb8a512428a6e21364b0c374df45503473c5242a253")
	// 3
	// ciphertext, err := hex.DecodeString("69dda8455c7dd4254bf353b773304eec0ec7702330098ce7f7520d1cbbb20fc388d1b0adb5054dbd7370849dbf0b88d393f252e764f1f5f7ad97ef79d59ce29f5f51eeca32eabedd9afa9329")
	// 4
	ciphertext, err := hex.DecodeString("770b80259ec33beb2561358a9f2dc617e46218c0a53cbeca695ae45faa8952aa0e311bde9d4e01726d3184c34451")
	if err != nil {
		log.Fatal(err)
	}

	// 1, 2
	// key, err := hex.DecodeString("140b41b22a29beb4061bda66b6747e14")
	// 3, 4
	key, err := hex.DecodeString("36f18357be4dbd77f050515c73fcf9f2")
	if err != nil {
		log.Fatal(err)
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	if len(ciphertext) < aes.BlockSize {
		panic("ciphertext too short")
	}

	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]

	fmt.Printf("Ciphertext size: %d\nAES block size:  %d\n", len(ciphertext), aes.BlockSize)
	// if len(ciphertext)%aes.BlockSize != 0 {
	// 	panic("ciphertext is not a multiple of the block size")
	// }

	// 1, 2
	// mode := cipher.NewCBCDecrypter(block, iv)
	// mode.CryptBlocks(plaintext, ciphertext)

	// 3, 4
	stream := cipher.NewCTR(block, iv)
	stream.XORKeyStream(plaintext, ciphertext)

	fmt.Printf("%s\n", plaintext)
}
