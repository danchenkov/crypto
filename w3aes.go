package main

import (
// "fmt"
)

func main() {
}

// 00000000000000000000000000000000
// 00000000000000000000000000000001
// 66e94bd4ef8a2c3b884cfa59ca342b2e//E(0,0)
// 66e94bd4ef8a2c3b884cfa59ca342b2f//E(0,0)^1
// E(0,0)^0 = E(1,m)^1
// m = D(1,E(0,0)^1)
// b0ddfe68d46f4f88c46e3b9892000c39//D(1,E(0,0)^1)

// E(0,0) = 66e94bd4ef8a2c3b884cfa59ca342b2e
// E(0,0)^1 = 66e94bd4ef8a2c3b884cfa59ca342b2f
// a17e9f69e4f25a8b8620b4af78eefd6f//E(1,1)
// c797d4bd0b7876b00e6c4ef6b2dad641//E(1,1)^E(0,0)
