package main

import "fmt"

func main() {
	code := make([][]int, 4)

	//                k1 ^ k2     k1          k2 ^ !k1    !k1
	// code[0] = []int{0x290b6e3a, 0x39155d6f, 0xd6f491c5, 0xb645c008}
	code[0] = []int{0x2d1cfa42, 0xc0b1d266, 0xeea6e3dd, 0xb2146dd0}
	// code[0] = []int{0x4af53267, 0x1351e2e1, 0x87a40cfa, 0x8dd39154}
	// code[0] = []int{0x5f67abaf, 0x5210722b, 0xbbe033c0, 0x0bc9330e}
	// code[0] = []int{0x7b50baab, 0x07640c3d, 0xac343a22, 0xcea46d60}
	// code[0] = []int{0x7c2822eb, 0xfdc48bfb, 0x325032a9, 0xc5e2364b}
	// code[0] = []int{0x9f970f4e, 0x932330e4, 0x6068f0b1, 0xb645c008}
	// code[0] = []int{0xe86d2de2, 0xe1387ae9, 0x1792d21d, 0xb645c008}
	// code[0] = []int{0x7b50baab, 0x07640c3d, 0xac343a22, 0xcea46d60}
	code[1] = []int{0x9f970f4e, 0x932330e4, 0x6068f0b1, 0xb645c008}
	// code[2] = []int{0x4af53267, 0x1351e2e1, 0x87a40cfa, 0x8dd39154}
	code[2] = []int{0x7c2822eb, 0xfdc48bfb, 0x325032a9, 0xc5e2364b}
	code[3] = []int{0x9d1a4f78, 0xcb28d863, 0x75e5e3ea, 0x773ec3e6}

	for i, _ := range code {
		fmt.Printf("%d: %x\n", i, code[i][0]^code[i][2])
	}
}

// In this letter I make some remarks on a general principle relevant to enciphering in general and my machine.
// The most direct computation would be for the enemy to try all 2^r possible keys, one by one.
// An enciphering-deciphering machine (in general outline) of my invention has been sent to your organization.
// We see immediately that one needs little information to begin to break down the process.
// If qualified opinions incline to believe in the exponential conjecture, then I think we cannot afford not to make use of it.
// The significance of this general conjecture, assuming its truth, is easy to see. It means that it may be feasible to design ciphers that are effectively unbreakable.
