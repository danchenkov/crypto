// # Suppose you are told that the one time pad encryption of the message "attack at dawn" is 09e1c5f70a65ac519458e7e53f36
// # (the plaintext letters are encoded as 8-bit ASCII and the ciphertext is written in hex).
// # What would be the one time pad encryption of the message "attack at dusk" under the same OTP key?
package main

import (
	"encoding/hex"
	"errors"
	"fmt"
	"log"
)

func otp(a, b []byte) ([]byte, error) {
	if len(a) != len(b) {
		return nil, errors.New("OTP: can't xor slices of different size")
	}
	c := make([]byte, len(a))
	for i, va := range a {
		// fmt.Printf("%2d:  %02x xor %02x = %02x\n", i, va, b[i], va ^ b[i])
		c[i] = va ^ b[i]
	}
	return c, nil
}

func main() {
	true_message := "attack at dawn"
	fake_message := "attack at dusk"
	true_encryption := "09e1c5f70a65ac519458e7e53f36"
	// true_encryption := "6c73d5240a948c86981bc294814d"

	encryption, err := hex.DecodeString(true_encryption)
	if err != nil {
		log.Fatal(err)
	}

	// fmt.Printf("%x\n", encryption)
	key, err := otp(encryption, []byte(true_message))
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("M: %x\n", []byte(true_message))
	fmt.Printf("E: %x\n", encryption)
	fmt.Printf("K: %x\n", key)

	fake_encryption, err := otp(key, []byte(fake_message))
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("F: %x\n", []byte(fake_message))
	fmt.Printf("Q: %x\n", fake_encryption)

}

// def hex_string(ascii_string)
//   hex_string = ''
//   ascii_string.chars { |c| hex_string << sprintf("%02x", c.ord) }
//   hex_string
// end

// def ascii_string(hex_string)
//   ascii_string = ''
//   hex_string.scan(/..\s*/).each { |e| ascii_string << e.to_i(16).chr }
//   ascii_string
// end

// def otp(msg,key)
//   # both msg and key are hex-strings, e.g. 'ef04ab2751d9'
//   return false if msg.class != String or key.class != String or msg.size%2 != 0 or msg.size != key.size
//   key_array = key.scan(/..\s*/)
//   otp = ''
//   msg.scan(/..\s*/).each { |m| otp << sprintf("%02x", m.to_i(16) ^ key_array.shift.to_i(16)) }
//   otp
// end

// # figuring out the key
// # true_message.chars { |c| true_message_array << c.unpack('H2').first.to_i(16) }
// puts "M: " + true_message.split('').join(' ')
// puts "H: " + hex_string(true_message)
// # encr_message.scan(/../).each { |e| encr_message_array << e.to_i(16) }
// puts "E: " + true_encryption
// key = otp(true_encryption, hex_string(true_message))
// puts "K: " + key
// puts "F: " + fake_message.split('').join(' ')
// puts "H: " + hex_string(fake_message)
// puts "Q: " + otp(key, hex_string(fake_message))
// # if true_message_array.size == encr_message_array.size
// #   true_message_array.each do |t|
// #     e = encr_message_array.pop
// #     k = t^e
// #     key_array << k
// #     key << k.to_s(16)
// #   end
// # end
// # puts key

// # new_encrypted_string = ''
// # new_encrypted_array = []
// # fake_message.chars { |c| fake_message_array << c.unpack('H2').first.to_i(16) }
// # puts fake_message.split('').join(' ')
// # if fake_message_array.size == key_array.size
// #   fake_message_array.each do |f|
// #     k = key_array.pop
// #     c = f ^ k
// #     new_encrypted_string << sprintf("%02x ", c)
// #   end
// # end
// # puts new_encrypted_string
