package main

import (
	"crypto/aes"
	"encoding/hex"
	// "crypto/cipher"
	"fmt"
	"log"
)

// func (block ADCBCBlock) BlockSize() int {
//   return len(block)
// }

// func (block ADCBCBlock) CryptBlocks(dst, src []byte) {
//   dst = src
// }

// func (b Block, iv []byte) BlockMode {

// }

type ADCBCMode

func main() {
	aes_key, _ := hex.DecodeString("c9af4cb13d1eb9e9ac5d7fabecfedfbd")
	encrypted, _ := hex.DecodeString("7e19ea5e26eead372e6edebd1ab5c116")
	decrypted := make([]byte, 16)

	block, err := aes.NewCipher(aes_key)
	if err != nil {
		log.Fatalln("Error occured: " + err.Error())
	}

	fmt.Printf("Block size: %d\n", block.BlockSize())
	block.Decrypt(decrypted, encrypted)
	fmt.Printf("Decrypted block: %s\n", hex.EncodeToString(decrypted))
}
