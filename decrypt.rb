#!/usr/bin/ruby
# encoding: UTF-8

SPACE_CODE = 0x20

CIPHERTEXTS = ['315c4eeaa8b5f8aaf9174145bf43e1784b8fa00dc71d885a804e5ee9fa40b16349c146fb778cdf2d3aff021dfff5b403b510d0d0455468aeb98622b137dae857553ccd8883a7bc37520e06e515d22c954eba5025b8cc57ee59418ce7dc6bc41556bdb36bbca3e8774301fbcaa3b83b220809560987815f65286764703de0f3d524400a19b159610b11ef3e',
'234c02ecbbfbafa3ed18510abd11fa724fcda2018a1a8342cf064bbde548b12b07df44ba7191d9606ef4081ffde5ad46a5069d9f7f543bedb9c861bf29c7e205132eda9382b0bc2c5c4b45f919cf3a9f1cb74151f6d551f4480c82b2cb24cc5b028aa76eb7b4ab24171ab3cdadb8356f',
'32510ba9a7b2bba9b8005d43a304b5714cc0bb0c8a34884dd91304b8ad40b62b07df44ba6e9d8a2368e51d04e0e7b207b70b9b8261112bacb6c866a232dfe257527dc29398f5f3251a0d47e503c66e935de81230b59b7afb5f41afa8d661cb',
'32510ba9aab2a8a4fd06414fb517b5605cc0aa0dc91a8908c2064ba8ad5ea06a029056f47a8ad3306ef5021eafe1ac01a81197847a5c68a1b78769a37bc8f4575432c198ccb4ef63590256e305cd3a9544ee4160ead45aef520489e7da7d835402bca670bda8eb775200b8dabbba246b130f040d8ec6447e2c767f3d30ed81ea2e4c1404e1315a1010e7229be6636aaa',
'3f561ba9adb4b6ebec54424ba317b564418fac0dd35f8c08d31a1fe9e24fe56808c213f17c81d9607cee021dafe1e001b21ade877a5e68bea88d61b93ac5ee0d562e8e9582f5ef375f0a4ae20ed86e935de81230b59b73fb4302cd95d770c65b40aaa065f2a5e33a5a0bb5dcaba43722130f042f8ec85b7c2070',
'32510bfbacfbb9befd54415da243e1695ecabd58c519cd4bd2061bbde24eb76a19d84aba34d8de287be84d07e7e9a30ee714979c7e1123a8bd9822a33ecaf512472e8e8f8db3f9635c1949e640c621854eba0d79eccf52ff111284b4cc61d11902aebc66f2b2e436434eacc0aba938220b084800c2ca4e693522643573b2c4ce35050b0cf774201f0fe52ac9f26d71b6cf61a711cc229f77ace7aa88a2f19983122b11be87a59c355d25f8e4',
'32510bfbacfbb9befd54415da243e1695ecabd58c519cd4bd90f1fa6ea5ba47b01c909ba7696cf606ef40c04afe1ac0aa8148dd066592ded9f8774b529c7ea125d298e8883f5e9305f4b44f915cb2bd05af51373fd9b4af511039fa2d96f83414aaaf261bda2e97b170fb5cce2a53e675c154c0d9681596934777e2275b381ce2e40582afe67650b13e72287ff2270abcf73bb028932836fbdecfecee0a3b894473c1bbeb6b4913a536ce4f9b13f1efff71ea313c8661dd9a4ce',
'315c4eeaa8b5f8bffd11155ea506b56041c6a00c8a08854dd21a4bbde54ce56801d943ba708b8a3574f40c00fff9e00fa1439fd0654327a3bfc860b92f89ee04132ecb9298f5fd2d5e4b45e40ecc3b9d59e9417df7c95bba410e9aa2ca24c5474da2f276baa3ac325918b2daada43d6712150441c2e04f6565517f317da9d3',
'271946f9bbb2aeadec111841a81abc300ecaa01bd8069d5cc91005e9fe4aad6e04d513e96d99de2569bc5e50eeeca709b50a8a987f4264edb6896fb537d0a716132ddc938fb0f836480e06ed0fcd6e9759f40462f9cf57f4564186a2c1778f1543efa270bda5e933421cbe88a4a52222190f471e9bd15f652b653b7071aec59a2705081ffe72651d08f822c9ed6d76e48b63ab15d0208573a7eef027',
'466d06ece998b7a2fb1d464fed2ced7641ddaa3cc31c9941cf110abbf409ed39598005b3399ccfafb61d0315fca0a314be138a9f32503bedac8067f03adbf3575c3b8edc9ba7f537530541ab0f9f3cd04ff50d66f1d559ba520e89a2cb2a83',
'32510ba9babebbbefd001547a810e67149caee11d945cd7fc81a05e9f85aac650e9052ba6a8cd8257bf14d13e6f0a803b54fde9e77472dbff89d71b57bddef121336cb85ccb8f3315f4b52e301d16e9f52f904']

PLAIN_MESSAGES = ['A report asserts that Afghanistan\’s largest financial institution existed primarily to allow a narrow clique tied to the Afghan government to siphon riches from depositors.',
'The French government’s plan to increase the beer tax by 160 percent has upset brewers, beer drinkers and bar owners, who ask why the proposal does not affect wine.',
'An American Psychiatric Association proposal to clarify diagnoses of personality disorders and better integrate them into clinical practice is meeting resistance.',

'After years of watching its international influence eroded by a slow-motion economic decline, the pacifist nation of Japan is trying to raise its profile in a new way, offering military aid for the first time in decades and displaying its own armed forces in an effort to build regional alliances and shore up other countries’ defenses to counter a rising China.',
'And after stepping up civilian aid programs to train and equip the coast guards of other nations, Japanese defense officials and analysts say, Japan could soon reach another milestone: beginning sales in the region of military hardware like seaplanes, and perhaps eventually the stealthy diesel-powered submarines considered well suited to the shallow waters where China is making increasingly assertive territorial claims.',
'Taken together those steps, while modest, represent a significant shift for Japan, which had resisted repeated calls from the United States to become a true regional power for fear that doing so would move it too far from its postwar pacifism. The country’s quiet resolve to edge past that reluctance and become more of a player comes as the United States and China are staking their own claims to power in Asia, and as jitters over China’s ambitions appear to be softening bitterness toward Japan among some Southeast Asian countries trampled last century in its quest for colonial domination.',

'The driver for Japan’s shifting national security strategy is its tense dispute with China over uninhabited islands in the East China Sea that is feeding Japanese anxiety that the country’s relative decline - and the financial struggles of its traditional protector, the United States - are leaving Japan increasingly vulnerable.',
'Japan’s moves do not mean it might transform its military, which serves a purely defensive role, into an offensive force anytime soon. The public has resisted past efforts by some politicians to revamp Japan’s pacifist constitution, and the nation’s vast debt will limit how much military aid it can extend.',
'Japanese leaders have met the Chinese challenge over the islands known as the Senkaku in Japan and the Diaoyu in China with an uncharacteristic willingness to push back, and polls show the public increasingly agrees. Both major political parties are also talking openly about instituting a more flexible reading of the constitution that would allow Japan to come to the defense of allies - shooting down any North Korean missile headed for the United States, for instance - blurring the line between an offensive and defensive force.',

'Japanese officials say their strategy is not to begin a race for influence with China, but to build up ties with other nations that share worries about their imposing neighbor. They acknowledge that even building the capacity of other nations’ coast guards is a way of strengthening those countries’ ability to stand up to any Chinese threat.',
'After years of gently prodding hospitals to make sure discharged patients do not need to return, the federal government is now using its financial muscle to discourage readmissions.']


# STRING METHODS

def hex_string(ascii_string)
  hex_string = ''
  ascii_string.chars { |c| hex_string << sprintf("%02x", c.ord) }
  hex_string
end

def ascii_string(hex_string)
  ascii_string = ''
  hex_string.scan(/..\s*/).each { |e| ascii_string << e.to_i(16).chr }
  ascii_string
end

def otp_hex_string(hex_msg, hex_key)
  # both msg and key are hex-strings, e.g. 'ef04ab2751d9'
  return false if hex_msg.class != String or hex_key.class != String or hex_msg.size%2 != 0 or hex_msg.size != hex_key.size
  hex_key_array = hex_key.scan(/..\s*/)
  hex_otp = ''
  hex_msg.scan(/..\s*/).each { |m| hex_otp << sprintf("%02x", m.to_i(16) ^ hex_key_array.shift.to_i(16)) }
  hex_otp
end

def xor_stings(one, two)     # xor two strings of different lengths
  (a,b) = one.size > two.size ? [one.slice(0, two.size), two] : [one, two.slice!(0, one.size)]
  strxor = ''
  0.upto(a.size-1) do |i|
    strxor << (a[i].ord ^ b[i].ord).chr
  end
  strxor
end

# SYMBOL METHODS

def hex(ascii_symbol)
  sprintf("%02x", ascii_symbol.ord)
end

def asc(hex_symbol)
  hex_symbol.to_i(16)
end

# ASCII METHODS

def random(size=16)
  open("/dev/random").read(size)
end

def encrypt(key, msg)
  puts "Encrypting message " + msg
  strxor(key, msg)
end

# ASCII SYMBOL METHODS

def alpha(hex_one, hex_two, ascii_code)
  return nil if ( hex_one.nil? || hex_two.nil? || ascii_code.nil? )
  symbol = asc(hex_one) ^ asc(hex_two) ^ ascii_code
  symbol >= 0x41 && symbol <= 0x5a || symbol >= 0x61 && symbol <= 0x7a
end

# ARRAY METHODS

def min(arr)
  min = arr.shift
  arr.each { |a| min = a if min > a }
  min
end

def max(arr)
  max = arr.shift
  arr.each { |a| max = a if max < a }
  max
end

def max_size(arr)
  max = arr.shift.size
  arr.each { |a| max = a.size if max < a.size }
  max
end

# MASS METHODS

def guess_space(line_index, position, code)
  curline = CIPHERTEXTS[line_index].scan(/..\s*/)
  probability = 0
  CIPHERTEXTS.each_index do |i| # iterate through lines
    next if i == line_index
    line = CIPHERTEXTS[i].scan(/..\s*/)
    if alpha(curline[position], line[position], code)
      probability += 1
    end
  end
  probability = (probability+0.0)/ CIPHERTEXTS.size
end

def force_decoding(line_index, position, symbol, cipher, plaintexts)
  code = symbol.ord
  decoded_line_array = CIPHERTEXTS[line_index].scan(/..\s*/)
  cipher[position] = asc(decoded_line_array[position]) ^ code
  # puts "Line #{line_index}, position #{position}"
  # puts "Forced symbol: #{symbol}"
  # puts "Encrypted line is #{CIPHERTEXTS[line_index]}"
  # puts "Code in position #{position} is #{decoded_line_array[position]}"
  # puts "Cipher in position #{position} is #{hex(cipher[position])}"
  CIPHERTEXTS.each_index do |i|
    if i == line_index
      plaintexts[i][position] = symbol
    else
      line_array = CIPHERTEXTS[i].scan(/..\s*/)
      plaintexts[i][position] = (asc(line_array[position]) ^ cipher[position]).chr
      # puts "Line #{i}[#{position}]: #{plaintexts[i][position]}"
    end
  end
end

cipher_length = max_size(CIPHERTEXTS)/2
cipher = Array.new(cipher_length)
plaintexts = Array.new(CIPHERTEXTS.size)
plaintexts.each_index do |i|
  plaintexts[i] = '*'*(CIPHERTEXTS[i].size / 2)
end

# puts cipher.inspect
0.upto(CIPHERTEXTS.size-1) do |i| # iterating through ciphertext lines
  0.upto(CIPHERTEXTS[i].size / 2 - 1) do |j| # iterating through symbols
    probability = guess_space(i, j, SPACE_CODE)
    if probability >= 0.85 # space found
      plaintexts[i][j] = ' '
      line = CIPHERTEXTS[i].scan(/..\s*/)
      cipher[j] = asc(line[j]) ^ ' '.ord
      0.upto(CIPHERTEXTS.size-1) do |k| # iterating through lines again
        next if k == i
        cline = CIPHERTEXTS[k].scan(/..\s*/)
        plaintexts[k][j] = (asc(cline[j]) ^ cipher[j]).chr
      end
    end
    # puts "Probability that #{j}'s symbol of line #{i} is SPACE is: #{p}" if p > 0.8
  end
end

# puts
# puts cipher.inspect

# 0.upto(CIPHERTEXTS.size-1) do |i| # iterating through ciphertext lines
#   puts plaintexts[i]
# end

force_decoding(3,   2, 'u', cipher, plaintexts)
force_decoding(0,   3, 'e', cipher, plaintexts)
force_decoding(7,   5, 'i', cipher, plaintexts)
force_decoding(7,   7, 'a', cipher, plaintexts)
force_decoding(0,   9, 'l', cipher, plaintexts)
force_decoding(0,  10, 'd', cipher, plaintexts)
force_decoding(0,  13, 'r', cipher, plaintexts)
force_decoding(0,  14, 'o', cipher, plaintexts)
force_decoding(0,  19, 'y', cipher, plaintexts)
force_decoding(1,  20, ' ', cipher, plaintexts)
force_decoding(8,  21, 'c', cipher, plaintexts)
force_decoding(7,  22, 'p', cipher, plaintexts)
force_decoding(8,  23, 'i', cipher, plaintexts)
force_decoding(8,  24, 'o', cipher, plaintexts)
force_decoding(8,  25, 'n', cipher, plaintexts)
force_decoding(8,  26, 'a', cipher, plaintexts)
force_decoding(8,  27, 'r', cipher, plaintexts)
force_decoding(8,  28, 'y', cipher, plaintexts)
force_decoding(7,  30, 'h', cipher, plaintexts)
force_decoding(7,  31, 'e', cipher, plaintexts)
force_decoding(4,  32, 'p', cipher, plaintexts)
force_decoding(4,  33, 'h', cipher, plaintexts)
force_decoding(4,  34, 'y', cipher, plaintexts)
force_decoding(0,  35, ' ', cipher, plaintexts)
force_decoding(2,  36, 'c', cipher, plaintexts)
force_decoding(2,  38, 'y', cipher, plaintexts)
force_decoding(2,  39, 'p', cipher, plaintexts)
force_decoding(2,  40, 't', cipher, plaintexts)
force_decoding(2,  41, 'i', cipher, plaintexts)
force_decoding(2,  42, 'o', cipher, plaintexts)
force_decoding(2,  43, 'n', cipher, plaintexts)
force_decoding(1,  44, 'o', cipher, plaintexts)
force_decoding(1,  46, 'r', cipher, plaintexts)
force_decoding(5,  49, 'w', cipher, plaintexts)
force_decoding(9,  50, ' ', cipher, plaintexts)
force_decoding(9,  51, 'n', cipher, plaintexts)
force_decoding(9,  52, 'e', cipher, plaintexts)
force_decoding(9,  53, 'v', cipher, plaintexts)
force_decoding(9,  54, 'e', cipher, plaintexts)
force_decoding(9,  55, 'r', cipher, plaintexts)
force_decoding(9,  56, ' ', cipher, plaintexts)
force_decoding(1,  57, ' ', cipher, plaintexts)
force_decoding(9,  58, 's', cipher, plaintexts)
force_decoding(9,  59, 'e', cipher, plaintexts)
force_decoding(9,  60, ' ', cipher, plaintexts)
force_decoding(9,  61, 't', cipher, plaintexts)
force_decoding(9,  62, 'h', cipher, plaintexts)
force_decoding(9,  63, 'e', cipher, plaintexts)
force_decoding(9,  64, ' ', cipher, plaintexts)
force_decoding(9,  65, 'k', cipher, plaintexts)
force_decoding(9,  66, 'e', cipher, plaintexts)
force_decoding(9,  67, 'y', cipher, plaintexts)
force_decoding(9,  68, ' ', cipher, plaintexts)
force_decoding(9,  69, 'm', cipher, plaintexts)
force_decoding(9,  70, 'o', cipher, plaintexts)
force_decoding(9,  71, 'r', cipher, plaintexts)
force_decoding(9,  72, 'e', cipher, plaintexts)
force_decoding(9,  73, ' ', cipher, plaintexts)
force_decoding(9,  74, 't', cipher, plaintexts)
force_decoding(9,  75, 'h', cipher, plaintexts)
force_decoding(9,  76, 'a', cipher, plaintexts)
force_decoding(9,  77, 'n', cipher, plaintexts)
force_decoding(9,  78, ' ', cipher, plaintexts)
force_decoding(9,  79, 'o', cipher, plaintexts)
force_decoding(9,  80, 'n', cipher, plaintexts)
force_decoding(9,  81, 'c', cipher, plaintexts)
force_decoding(9,  82, 'e', cipher, plaintexts)

print '   '
0.upto(132) { |i| print i%10 }
puts
0.upto(CIPHERTEXTS.size-1) do |i| # iterating through ciphertext lines
  puts "#{i}: #{plaintexts[i]}"
end


