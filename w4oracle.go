package main

import (
  // "flag"
  "fmt"
  // "io"
  "io/ioutil"
  // "log"
  // "net"
  // "crypto/cipher"
  "encoding/hex"
  "net/http"
  // "net/http/httptest"
  // "net/url"
  "os"
  "runtime"
  "unsafe"
)

// The Magic Words are Squeamish Ossifrage

const CLR_R = "\x1b[31;1m"
const CLR_G = "\x1b[32;1m"
const CLR_N = "\x1b[0m"

const wordSize = int(unsafe.Sizeof(uintptr(0)))
const supportsUnaligned = runtime.GOARCH == "386" || runtime.GOARCH == "amd64"

var (
  baseurl = "http://crypto-class.appspot.com/"
  request = "po?er="
)

func xor(args ...[]byte) []byte {
  total := len(args)
  if total < 1 {
    return nil
  }
  length := 0
  longest := 0
  for i, v := range args {
    if len(v) > length {
      length = len(v)
      longest = i
    }
  }
  dest := make([]byte, length)
  copy(dest, args[longest])
  for i := 0; i < length; i++ {
    for j := 0; j < total; j++ {
      if j != longest {
        cl := len(args[j])
        if i < cl {
          dest[length-i-1] = dest[length-i-1] ^ args[j][cl-i-1]
        }
      }
    }
  }
  return dest
}

func main() {
  // iv, _ := hex.DecodeString("f20bdba6ff29eed7b046d1df9fb70000")

  blocks := 2
  ciphertext := make([][]byte, blocks)
  for b := 0; b < blocks; b++ {
    ciphertext[b] = make([]byte, 16)
  }

  ciphertext[0], _ = hex.DecodeString("f20bdba6ff29eed7b046d1df9fb70000")
  ciphertext[1], _ = hex.DecodeString("58b1ffb4210a580f748b4ac714c001bd")
  // ciphertext[2], _ = hex.DecodeString("4a61044426fb515dad3f21f18aa577c0")
  // ciphertext[3], _ = hex.DecodeString("bdf302936266926ff37dbf7035d5eeb4")

  // AES block size: 128 bits, i.e. 16 bytes
  // Message is 3 blocks, 48 bytes

  current_block := 0

  original_block := make([]byte, 16)
  copy(original_block, ciphertext[current_block])
  guess := make([]byte, 16)
  decoded := make([]byte, 16)
  padding := make([]byte, 16)
  // intermediate := make([]byte, 16)

  guess = []byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
  for byte_counter := 9; byte_counter < 0x10; byte_counter++ {
    current_byte := 0xF - byte_counter
    fmt.Println("CURRENT BYTE:", current_byte)

    // ciphertext[1][0xF] = "c0"
    // guess[0xF] = "09"
    // guess[0xF] ^ ciphertext[1][0xF] ^ 01 ^ intermediate[0xF] => 01

    // intermediate[0xF] = byte(0xcb)

    // generate padding, for 0xF => "01", 0xE => "0202"

    for i := current_byte; i < 0x10; i++ {
      padding[i] = byte(0x10 - current_byte)
    }
    // fmt.Println("O:", hex.EncodeToString(original_block))
    // fmt.Println("P:", hex.EncodeToString(padding))

    for counter := 0; counter < 0x100; counter++ {
      guess[current_byte] = byte(counter)
      // for j := current_byte + 1; j < 0x10; j++ {
      //   guess[j] = intermediate[j]
      // }

      fmt.Printf("G: %s\n", hex.EncodeToString(guess))
      fmt.Printf("O: %s\n", hex.EncodeToString(original_block))
      fmt.Printf("P: %s\n", hex.EncodeToString(padding))
      // fmt.Printf("C: %s\n", hex.EncodeToString(ciphertext[current_block]))
      // fmt.Print("C: ", hex.EncodeToString(iv)+hex.EncodeToString(ciphertext[0])+hex.EncodeToString(ciphertext[1])+hex.EncodeToString(ciphertext[2]))
      ciphertext[current_block] = xor(original_block, guess, padding)
      fmt.Printf("C: %s", hex.EncodeToString(xor(original_block, guess, padding)))

      // response, err := http.Get(baseurl + request + hex.EncodeToString(iv) + hex.EncodeToString(ciphertext[0]) + hex.EncodeToString(ciphertext[1]))
      response, err := http.Get(baseurl + request + hex.EncodeToString(ciphertext[0]) + hex.EncodeToString(ciphertext[1]))
      if err != nil {
        fmt.Printf("%s", err)
        os.Exit(1)
      } else {
        defer response.Body.Close()
        _, err := ioutil.ReadAll(response.Body)
        if err != nil {
          fmt.Printf("%s", err)
          os.Exit(1)
        }
        if response.StatusCode == 404 {
          // fmt.Printf("%02x: %s%s%s\n", counter, CLR_G, response.Status, CLR_N)
          decoded[current_byte] = byte(counter)
          // fmt.Print("C: ", hex.EncodeToString(iv)+hex.EncodeToString(ciphertext[0])+hex.EncodeToString(ciphertext[1])+hex.EncodeToString(ciphertext[2]))
          // fmt.Printf("C: %s", hex.EncodeToString(ciphertext[1]))
          fmt.Printf(" %0x %s✓%s\n\n", byte(counter), CLR_G, CLR_N)
          fmt.Printf("I: %s\n", hex.EncodeToString(xor(original_block, guess)))
          // fmt.Printf(" %0x %s✓%s (%s%s%s)\n", byte(counter), CLR_G, CLR_N, CLR_G, response.Status, CLR_N)
          // fmt.Println("O:", hex.EncodeToString(iv), hex.EncodeToString(ciphertext[0]), hex.EncodeToString(original_block), hex.EncodeToString(ciphertext[2]))
          // fmt.Println("S:", hex.EncodeToString(iv), hex.EncodeToString(ciphertext[0]), hex.EncodeToString(ciphertext[1]), hex.EncodeToString(ciphertext[2]))
          // fmt.Println("O:", hex.EncodeToString(iv), hex.EncodeToString(original_block), hex.EncodeToString(ciphertext[1]))
          // fmt.Println("S:", hex.EncodeToString(iv), hex.EncodeToString(ciphertext[0]), hex.EncodeToString(ciphertext[1]))
          fmt.Println("O:", hex.EncodeToString(ciphertext[0]), hex.EncodeToString(original_block))
          fmt.Println("S:", hex.EncodeToString(ciphertext[0]), hex.EncodeToString(ciphertext[1]))
          fmt.Println()
          fmt.Println("D:", hex.EncodeToString(decoded))
          break
        } else {
          fmt.Printf(" %0x %s✗%s\n\n", byte(counter), CLR_R, CLR_N)
          // fmt.Printf(" %0x %s✗%s (%s%s%s)\n", byte(counter), CLR_R, CLR_N, CLR_R, response.Status, CLR_N)
        }
      }
    }
  }

  fmt.Printf("Plaintext: '%s'\n", string(decoded))
}
